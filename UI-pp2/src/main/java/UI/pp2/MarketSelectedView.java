package UI.pp2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class MarketSelectedView extends JFrame {
	private static final long serialVersionUID = 1L;
	private PropertyChangeSupport changes = new PropertyChangeSupport(this);
	private JLabel marketName;
	private JFrame frame;
	private JPanel panel;
	private JButton changeButton;
	
	public MarketSelectedView() {
		this.initialize();
	}
	
	public void setMarketName(String marketName) {
		this.marketName.setText("Mercado seleccionado : " + marketName);
	}
	
	public void addComponents() {
		panel.removeAll();
		JLabel message = new JLabel("Bot operando ....");
		message.setBounds(10, 10, 300, 30);
		panel.add(message);
		marketName = new JLabel("");
		marketName.setBounds(10, 40, 500, 30);
		panel.add(marketName);
		frame.setBounds(100, 100, 300, 150);
		panel.setBounds(0, 0, 300, 150);
		addChangeButton();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setLocationRelativeTo(null);
		frame.setTitle("Configuración del Bot");
		frame.setVisible(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		panel = new JPanel();
		panel.setLayout(null);
		frame.getContentPane().add(panel);
		addComponents();
	}
	
	private void addChangeButton() {
		changeButton = new JButton("Cambiar Mercado");
		changeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				changes.firePropertyChange("updateMarket", null, null);
			}
		});
		changeButton.setBounds(10, 80, 170, 30);
		panel.add(changeButton);
	}

	
	public void show() {
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exit();
			}
		});
		this.frame.setVisible(true);
	}

	public boolean showConfirmation(String message) {
		int confirm = JOptionPane.showOptionDialog(null, message, "Confirmación", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, null, null);
		return confirm == 0 ? true : false;
	};

	public void showMessageDialog(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	public void exit() {
		boolean confirm = showConfirmation("¿Estás seguro de que quieres salir?");
		if (confirm)
			System.exit(0);
	}

	public void hide() {
		frame.setVisible(false);
		frame.dispose();
	}

	public void addPropertyChangeListener(PropertyChangeListener l) {
		changes.addPropertyChangeListener(l);
	}
}
