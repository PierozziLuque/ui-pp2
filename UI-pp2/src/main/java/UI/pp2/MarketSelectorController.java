package UI.pp2;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import BiTrader.pp2.MarketSelector;

public class MarketSelectorController implements PropertyChangeListener{
	private MarketSelector marketSelector;
	private MarketSelectorView view;
	private MarketSelectedView selectedView;

	public MarketSelectorController(MarketSelectorView view, MarketSelector marketSelector) {
		this.marketSelector = marketSelector;
		this.view = view;
		this.selectedView = new MarketSelectedView();
		selectedView.addPropertyChangeListener(this);
		view.addPropertyChangeListener(this);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		String nameEvent = event.getPropertyName();
		switch (nameEvent) {
			case "marketSelected":
				selectMarket(event);
				break;
			case "updateMarket":
				updateMarket();
				break;
		}
	}
	
	private void selectMarket(PropertyChangeEvent event) {
		String selected = event.getNewValue().toString();
		marketSelector.setMarket(selected);
		view.hide();
		selectedView.show();
		selectedView.setMarketName(selected);
	}
	
	private void updateMarket() {
		selectedView.hide();
		view.show();
	}
}
