package UI.pp2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import BiTrader.pp2.MarketSelector;

public class MarketSelectorView extends JFrame {
	private PropertyChangeSupport changes = new PropertyChangeSupport(this);
	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private JPanel panel;
	private JButton acceptButton;
	private int x;
	private int y;
	private MarketSelector marketSelector;
	private JComboBox<String> comboBoxPadre;
	@SuppressWarnings("unused")
	private MarketSelectorController controller;
	
	public MarketSelectorView(MarketSelector marketSelector) {
		this.marketSelector = marketSelector;
		this.controller = new MarketSelectorController(this, marketSelector);
		this.initialize();
	}

	public void addClasses(List<String> names) {
		comboBoxPadre.removeAllItems();
		for(String className : names) {
			comboBoxPadre.addItem(className);
		}
	}

	public void addComponents() {
		List<String> nameMarkets = marketSelector.getNameMarkets();
		panel.removeAll();
		x = 10;
		y = 10;
		JLabel title = new JLabel("Seleccione  el Mercado a utilizar:");
		title.setBounds(x, y, 300, 30);

		comboBoxPadre = new JComboBox<String>();
		addClasses(nameMarkets);
		y += 40;
		comboBoxPadre.setBounds(x, y, 278, 24);
		panel.add(comboBoxPadre);
		y+=40;
		this.addButtons();
		panel.add(title);
		frame.setBounds(100, 100, 350, y + 40);
		panel.setBounds(0, 0, 350, y + 40);
	}

	private void addButtons() {
		this.addAcceptButton();
		this.addCancelButton();
		y+= 40;
	}

	private void addAcceptButton() {
		acceptButton = new JButton("Aceptar");
		acceptButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				changes.firePropertyChange("marketSelected", null, comboBoxPadre.getSelectedItem());
			}
		});
		acceptButton.setBounds(x, y, 100, 30);
		panel.add(acceptButton);
	}

	private void addCancelButton() {
		JButton cancelButton = new JButton("Cancelar");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				exit();
			}

		});
		cancelButton.setBounds(x + 110, y, 100, 30);
		panel.add(cancelButton);
	}

	private void initialize() {
		frame = new JFrame();
		frame.setLocationRelativeTo(null);
		frame.setTitle("Configuración del Bot");
		frame.setVisible(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		panel = new JPanel();
		panel.setLayout(null);
		frame.getContentPane().add(panel);
		addComponents();
	}
	
	public void show() {
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exit();
			}
		});
		this.frame.setVisible(true);
	}

	public boolean showConfirmation(String message) {
		int confirm = JOptionPane.showOptionDialog(null, message, "Confirmación", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, null, null);
		return confirm == 0 ? true : false;
	};

	public void showMessageDialog(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	public void exit() {
		boolean confirm = showConfirmation("¿Estás seguro de que quieres salir?");
		if (confirm)
			System.exit(0);
	}

	public void hide() {
		frame.setVisible(false);
		frame.dispose();
	}

	public void addPropertyChangeListener(PropertyChangeListener l) {
		changes.addPropertyChangeListener(l);
	}
}
